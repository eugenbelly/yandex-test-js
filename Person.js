// Этот компонент лучше назвать по другому. Никакую "персону" он не представляет.
// Он представляет "настроение". Так что, "MoodIndicator" было бы лучшим назанием.

class Person extends Component {
    constructor(name) {
        super();
        // Похоже что поле и аргумент "name" нигде не используется. От них следует избавиться
        this.name = name;
        this._happiness = 0;
        this._valueElement = document.querySelector(`.column__value-name`);
        this._iconElement = document.querySelector(`.column__value-icon`);
    }

    // Вместо всех 4х методов ниже, стоило бы сделать один метод. Примерно такой:
    // calculateMood(hasCat, hasRest, hasMoney) {
    //   ...
    // }

    hasCat() {
        return this._happiness++;
    }

    hasRest() {
        return this._happiness++;
    }

    hasMoney() {
        return this._happiness++;
    }

    isSunny() {
        const APIKey = '28c7d687accc7c75aabbc7fb71173feb';
        const city = 'Москва';
        const url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + APIKey;

        return fetch(url)
            .then(res => res.json())
            .then((res) => {
              console.log(this._happiness);
                if (res.main.temp - 273 > 15) {
                    // Здесь возвращается значение, которое еще не было увеличено на единицу.
                    // Чтобы было правильно, нужно использовать пре-, а не пост-инкремент:
                    // return ++this._happiness;
                    return this._happiness++;
                }
            });
      }


    // Здесь стоит реализовать template, как в других компонентах. Не надо
    // объявлять ".column__value-name" и ".column__value-icon" в HTML-файле
    // Зто нужно сделать здесь в темплейте
}
