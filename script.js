window.onload = () => {
	const FORM_WRAPPER = document.querySelector(`.column_type_input`);
	const ratingArray = [];
	let countedRating = 20;


  const renderSearch = (allItemsData) => {
    PageEnum.SiteWrapper.SEARCH.innerHTML = ``;

    const searchComponent = new Search();

    PageEnum.SiteWrapper.SEARCH.appendChild(searchComponent.render());

    searchComponent.onChange = (value) => {
      const filteredItems = allItemsData.filter((currentItem) => currentItem._names.includes(value));
      PageEnum.SiteWrapper.rating.innerHTML = ``;
      if (value === ``) {
        ratingRender(countedRating, allItemsData);
      } else {
        ratingUpdate(filteredItems);
      }
    };
  };

  const ratingRender = (ratingAmount, ratingArray) => {
    for (let i = 0; i < ratingAmount; i++) {
      ratingArray[i] = new PersonRating(returnRandomData());
    }
    ratingUpdate(ratingArray);
  };

  const ratingUpdate = (ratingArray) => {
    ratingArray.forEach((item) => {
      PageEnum.SiteWrapper.rating.appendChild(item.render());
    });
    if (ratingArray.length === 0) {
      PageEnum.SiteWrapper.rating.innerHTML = `Rating list is empty`
    }
  };

	const renderForm = () => {
		const formComponent = new Form();
		FORM_WRAPPER.appendChild(formComponent.render());

		formComponent.onSubmit = (evt) => {
			evt.preventDefault();
			// Очень много дублирования в 4 строках ниже. Стоит придумать как от этого избавиться.
			const name = document.querySelector(`input[name=name]`).value;
			const cat = document.querySelector(`input[name=cat]`).value;
			const rest = document.querySelector(`input[name=rest]`).value;
			const money = document.querySelector(`input[name=money]`).value;
			//Экземпляры классов должны называться с малой буквы
			const Man = new Person(name);
			if (cat === 'yes') {
				Man.hasCat();
			}
			if (rest === 'yes') {
				Man.hasRest();
			}
			if (money === 'yes') {
				Man.hasMoney();
			}
			Man.isSunny()
				.then((happiness) => {
				  // Слишком много дублирования "Man._iconElement.innerHTML"
          // Лучше сделать отдельную функцию для того чтобы сопоставлять цифру со смайлом.
          // Вот так:
          // Man._iconElement.innerHTML = getMoodEmoji(happiness)
          // function getMoodEmoji(happiness) {
          //   if (happiness > 3) return '😆'
          //   if (happiness < 2) return '☹'
          //   return '😐'
          // }
					// И вообще, этот функционал нужно перенести в сам класс Person -
					// в нем сделать метод calculateMood, который бы принимал
					// параметры hasCat, hasRest и hasMoney и автоматически обновлял
					// свое состояние и контент
					Man._valueElement.innerHTML = name;
					if (happiness === 4) {
						Man._iconElement.innerHTML = '😆';
					} else if (happiness === 3 || happiness === 2) {
						Man._iconElement.innerHTML = '😐';
					} else {
						Man._iconElement.innerHTML = '☹️';
					}
				});
		}
	};

	renderForm();
  renderSearch(ratingArray);
	ratingRender(countedRating, ratingArray);
};

// Поправить неоднородное форматирование. Где-то отступ 4 пробела, где-то 2, где-то это смешивается.
// Рекомендую везде использовать 2 отступа. Для исправления рекомендую работать в IDE (VSCode, Atom, IntelliJ),
// чтобы исправлять не вручную, а использовать встроенное средство автоформатирования.

// Решение не является полностью рабочим:
// 1. Поиск по пользователям не работает
// 2. У вопросов, с полями да\нет изначальное значение не установлено, но при сабмите оно интерпретируется как отрицательное.
// Чтобы не путать пользователя, нужно либо установить начальное значение на "нет", либо реализовать подсчет так, чтобы
// неотвеченные вопросы не учитывались

// Во всех файлах нужно поправить отступы

// В коде есть уязвимость: если в поле "Имя?" вписать какой-нибудь HTML то он прямо так и вставится, как HTML, а не как текст.
// Таким образом можно внедрить вредоносный скрипт.
// Чтобы это предотвратить, перед использованием данных любых текстовых полей, нужно их сначала экранровать от опасных символов.
// Для этого можно воспользоваться вот такой функцией: https://stackoverflow.com/a/48226843/4484547

// Нужно сделать компонент "UsersRatingList" и инкапсулировать в нем компоненты Search и PersonRating и логику для них,
// которая реализована здесь в функциях ratingRender, ratingUpdate и renderSearch

// Использование нижнего подчеркивания для обозначения приватных полей не только загрязняет код,
// но и не несет особого смысла - все равно, как мы видим, эти поля используются публично

// В некоторые компоненты обработчики (onChange, onSubmit) передается через сеттер.
// 1. Вместо сеттера его лучше передавать прямо в конструктор.
// Это так же поможет сократить количество методов в таких компонентах
// 2. Сеттеры - плохая практика. Их стоит избегать.

// Нужно исправить названия CSS-классов, почти все они не несут смысла.
