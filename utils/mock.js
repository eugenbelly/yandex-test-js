// 1. Данные стоит хранить не по колонкам, а по строкам, то есть в таком виде:
// const RATING_LIST = [
//   { name: 'John', rating: 1 },
//   { name: 'Jane', rating: 777 },
// ]
// 2. В names 3 значения, а в ratings 4 - тут явно ошибка
const RATING_LIST = {
  'names': [
    `User`,
    `Jason`,
    `WeatherBot`,
  ],
  'ratings': [
    10,
    9.4,
    2.6,
    6.5
  ]
};
